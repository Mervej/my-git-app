module.exports = function(app, bodyParser, request)
{
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({extended: true}))

	app.use(function(req, res, next) {
	  res.header("Access-Control-Allow-Origin", "*");
	  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	  next();
	});

	app.post('/', function(req, res){
		var postData = {
			grant_type : req.body.grant_type,
			username : req.body.username,
			password : req.body.password
		};
	  	console.log('post data is '+ postData.username);
		requestdata(postData,request,function(body, response){
			res.send(body);
			console.log('inside callback function'+Object.getOwnPropertyNames(body
				));
	  		res.end();
		});
	})
}

var url = 'https://gitlab.com/oauth/token';

var requestdata = function(data, request, cb){

	var headersOpt = {  
    		"content-type": "application/json",
	};

	request({
		method: 'post',
		url: url,
		form: data,
		headers: headersOpt,
		json: true
	}, function(error, response, body){
		if(error){
			console.log('error in post request'+error);
			return;
		}
		cb(body, response);
		console.log('response from inside request is '+ response.statusCode);
	});	
}
