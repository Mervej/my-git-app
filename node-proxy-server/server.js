const http = require('http');
const https = require('https');
const request = require('request');
const fs = require('fs');
const express = require('express');
const bodyParser = require('body-parser');

const hostname = '127.0.0.1';
const port = '8000';

const app = express();

require('./router/main')(app,bodyParser,request);
app.set('views','/views');

app.listen(port, () =>{
	console.log('server running');
})
