import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class RestProvider {

  cordova: any;

  constructor(public http: HttpClient) {
    console.log('Hello RestProvider Provider');
  }

  getData(url: string) {
	    return new Promise((resolve, reject) => {
			this.http.get(url)
				.subscribe(data => {
		      		resolve(data);	
		    	}
		    	,error => {
		    		reject('error');
		    	});
		});
	}

	postData(url: string, postData: any){
		return new Promise((resolve, reject) =>{
			url = 'http://127.0.0.1:8000';
   			this.http.post(url, postData, {
					  observe: 'response'
					})
				.subscribe(data => {
					resolve(data);
					},
				err => {
				    console.log('error in request ' + err);
				});
		});
	}
}
