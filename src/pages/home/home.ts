import { Component } from '@angular/core';
import { NavController,MenuController,NavParams,AlertController } from 'ionic-angular';
import { ProjectModel } from '../../model/model';
import { RestProvider } from '../../providers/rest/rest';
import { LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { LoginPage } from '../login/login';
import { AboutProjectPage } from '../about-project/about-project';
import { AboutUserPage } from '../about-user/about-user';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  userID: string;
  userName: string;
  url: string;
  postURl: string;
  Projects: any;
  loader: any;
  alert: any;
  userFound: boolean;
  
  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public menuCtrl: MenuController, public navParams: NavParams, public restProvider: RestProvider,public loadingCtrl: LoadingController, private storage: Storage) {
    this.userID = this.navParams.get("param1");
    this.userName = this.navParams.get("param2");
    this.menuCtrl.enable(true);
    this.loader = loadingCtrl.create();
    this.loader.present(this.loader);
    this.alert = this.alertCtrl.create();
    let postData = {
        grant_type : "password",
        username : this.userID,
        password : this.userName
    };
    
    storage.get('projectData')
      .then((val) =>{
        if(val != null) {
          this.userFound = true;
          this.loader.dismiss();
          this.Projects = val;
        }
        else{
          console.log("data not found");
          this.postURl = 'https://gitlab.com/oauth/token';
          this.postDataRequest(postData);
        }
      });
  }

  getProjectData(id: any) {
    this.url = 'https://gitlab.com/api/v4/users/'+id+'/projects';
    console.log('inside get 2', this.url);
    this.restProvider.getData(this.url)
      .then(data => {
            console.log('inside get request 2');
            this.loader.dismiss();
            this.storage.set('projectData',data);
            this.Projects = data;
      })
      .catch ((error)=>{
          this.alert.setTitle('Error while fetching projects');
          this.alert.addButton({
            text: 'Go Back',
            handler: () => {
                this.navCtrl.setRoot(LoginPage);
              }
          });
          this.loader.dismiss();
          this.alert.present();
        });
  }

  getData(token: any) {
    this.url = 'https://gitlab.com/api/v4/user/?access_token='+token; 
    console.log('inside get request 1',this.url);
    this.restProvider.getData(this.url)
      .then(data => {
        this.storage.set('userData', data);
        this.getProjectData(data.id);
      })
      .catch ((error)=>{
          console.log(error);
          this.alert.setTitle('Invalid Username or Password!!!');
          this.alert.addButton({
            text: 'Go Back',
            handler: () => {
                this.navCtrl.setRoot(LoginPage);
              }
          });
          this.loader.dismiss();
          this.alert.present();
        });
  }

  postDataRequest(data: any)
  {
    this.restProvider.postData(this.postURl , data)
      .then(data => {
          if(data.status == 200){
            console.log('inside post request');
            this.access_token = data.body.access_token;
            this.getData(this.access_token);
          }
          else{
            console.log('401 error');
            this.alert.setTitle('Invalid Username or Password!!!');
            this.alert.addButton({
              text: 'Go Back',
              handler: () => {
                  this.navCtrl.setRoot(LoginPage);
                }
            });
            this.loader.dismiss();
            this.alert.present();
          }

        })
      .catch(error=>{
          console.log("data error in inside " + error);
      }); 
  }

  goToProjectPage(projectID : string ){
    this.navCtrl.push(AboutProjectPage, {param1 : projectID});
  }
}
