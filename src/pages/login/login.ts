import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  id: string;
  password: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage) {
  	storage.clear();
    console.log("cleared");
  }

  goToHomePage(userId :string, password :string ){
	  this.navCtrl.setRoot(HomePage,{param1 : userId , param2 : password});
  }

}
