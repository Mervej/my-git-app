import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProjectModel } from '../../model/model';
import { RestProvider } from '../../providers/rest/rest';
import { LoadingController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-about-project',
  templateUrl: 'about-project.html',
})
export class AboutProjectPage {

  public param1 : string ;
  Projects: any = new ProjectModel();
  loader: any ;

  constructor(public navCtrl: NavController, public navParams: NavParams, public restProvider: RestProvider, public loadingCtrl: LoadingController) {
		this.loader = loadingCtrl.create();
		this.loader.present(this.loader);
	 	this.param1 = this.navParams.get("param1");
       console.log("parameters",this.param1);
       this.getData(this.param1);
  }

    getData(id:string ) {
    this.restProvider.getData('https://gitlab.com/api/v4/projects/'+id)
    .then(data => {
      this.loader.dismiss();
      this.Projects = data;
      console.log(this.Projects.description);
    });
  }

   goBack(){
        this.navCtrl.pop();
    } 

}
