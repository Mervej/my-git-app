import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { ProjectModel } from '../../model/model';
import { LoadingController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-about-user',
  templateUrl: 'about-user.html',
})

export class AboutUserPage {

  Projects: any = new ProjectModel();
  loader: any ;  

  constructor(public navCtrl: NavController, public menuCtrl: MenuController, public restProvider: RestProvider, public loadingCtrl: LoadingController) {
    this.loader = loadingCtrl.create();
    this.loader.present(this.loader);
	  this.menuCtrl.enable(true);
  	this.getData();
  }

  getData() {
    this.restProvider.getData('https://gitlab.com/api/v4/users/2066269')
    .then(data => {
      this.loader.dismiss();
      this.Projects = data;
    });
  }

}
