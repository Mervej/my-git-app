import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AboutUserPage } from './about-user';

@NgModule({
  declarations: [
    AboutUserPage,
  ],
  imports: [
    IonicPageModule.forChild(AboutUserPage),
  ],
})
export class AboutUserPageModule {}
