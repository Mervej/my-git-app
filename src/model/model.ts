export class ProjectModel {
  id: string;
  name: string;
  username: string;
  description: string;
  avatar_url: string;
  web_url: string;
  bio: string;
  created_at: string;
  location: string;
  linkedin: string;
}